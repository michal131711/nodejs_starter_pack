import * as HttpStatus from 'http-status-codes';

export const authorize = (req, res, next) => {
	try {
		if (!req.session.user) {
			throw {
				code: HttpStatus.UNAUTHORIZED, // 401
				message: "Nie jesteś zalogowany"
			}
		}
		next();
	} catch (exception) {
		res.render('users/login', {
			code: exception.code || HttpStatus.INTERNAL_SERVER_ERROR,
			message: exception.message,
		});
	}
};
