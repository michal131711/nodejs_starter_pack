import * as HttpStatus from 'http-status-codes';
import Roles from "../enums/Roles";
import {findUserById} from "../services/users";
import User from "../src/entity/User";

export const isAdmin = (req, res, next) => {
	permission(req, res, next, Roles.ROLE_ADMIN);
};

export const isClient = (req, res, next) => {
	permission(req, res, next, Roles.ROLE_CLIENT);
};

export const isSubscriber = (req, res, next) => {
	permission(req, res, next, Roles.ROLE_SUBSCRIBER);
};

export const hasPermission = (req, res, next) => {
	try {
		if (!req.session.status) {
			throw {
				code: HttpStatus.FORBIDDEN, // 403
				message: "You don't have permission to this action!"
			}
		}
		next();
	} catch (exception) {
		res.render('error', {
			code: exception.code || HttpStatus.INTERNAL_SERVER_ERROR,
			message: exception.message,
		});
	}
};

const permission = async (req, res, next, PERMISSION) => {
	try {
		const user: User = await findUserById(req.session.user.id);

		let hasPermission = false;
		for (const role of user._roles) {
			if (role._name === PERMISSION) {
				hasPermission = true;
			} else {
				res.render('error', {
					code: HttpStatus.FORBIDDEN,
					user: req.session.user,
					message: "Nie masz uprawnień poproś o administratora o nadanie uprawnień na tą sekcje"
				});
			}
		}

		req.session.status = req.session.status || hasPermission;
		next();
	} catch (exception) {
		res.render('error', {
			code: exception.code || HttpStatus.INTERNAL_SERVER_ERROR,
			message: exception.message,
		});
	}
};
