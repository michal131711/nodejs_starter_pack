import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	JoinTable,
	ManyToMany,
	BaseEntity,
	UpdateDateColumn,
	CreateDateColumn, PrimaryColumn
} from "typeorm";
import IRole from "../../interfaces/IRole";
import Role from "./Role";

@Entity("users")
export default class User extends BaseEntity{

	constructor(
		id: number | null,
		email: string,
		firstName: string,
		lastName: string,
		password: string,
		token: string,
	) {
		super();
		if (id) {
			this.id = id;
		}
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.token = token;
	}

	@PrimaryGeneratedColumn('increment')
	private readonly id: number;

	get _id(): number {
		return this.id;
	}

	@Column({ type: 'varchar', length: 250 })
	private email: string;

	get _email(): string {
		return this.email;
	}

	set _email(value: string) {
		this.email = value;
	}

	@Column({ type: 'varchar', length: 250 })
	private firstName: string;

	get _firstName(): string {
		return this.firstName;
	}

	set _firstName(value: string) {
		this.firstName = value;
	}

	@Column({ type: 'varchar', length: 250 })
	private lastName: string;

	get _lastName(): string {
		return this.lastName;
	}

	set _lastName(value: string) {
		this.lastName = value;
	}

	@Column({ type: 'varchar', length: 200 })
	private password: string;

	get _password(): string {
		return this.password;
	}

	set _password(value: string) {
		this.password = value;
	}

	@Column({ type: 'varchar', length: 500 })
	private token: string;

	get _token(): string {
		return this.token;
	}

	set _token(value: string) {
		this.token = value;
	}

	@ManyToMany(() => Role)
	@JoinTable()
	private roles: Role[];

	get _roles(): Role[] {
		return this.roles;
	}

	set _roles(value: Role[]) {
		this.roles = value;
	}

	get _getFullName(): string {
		return this.firstName + ' ' + this.lastName;
	}

	@CreateDateColumn()
	createdAt: Date;

	@UpdateDateColumn()
	updatedAt: Date;
};
