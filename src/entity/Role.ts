import {
	BaseEntity,
	Column,
	CreateDateColumn,
	Entity,
	PrimaryColumn,
	PrimaryGeneratedColumn,
	UpdateDateColumn
} from "typeorm";

@Entity("roles")
export default class Role extends BaseEntity{
	constructor(id: number | null, name: string) {
		super();
		if (id) this.id = id;
		this.name = name;
	}

	@PrimaryGeneratedColumn('increment')
	private readonly id: number;

	get _id() {
		return this.id;
	}

	@Column({ type: 'varchar', length: 250 })
	private name: string;

	get _name() {
		return this.name;
	}

	set _name(value) {
		this.name = value;
	}

	@CreateDateColumn()
	createdAt: Date;

	@UpdateDateColumn()
	updatedAt: Date;
};
