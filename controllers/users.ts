import * as bodyParser from 'body-parser';
import * as Joi from 'joi';
import * as HttpStatus from 'http-status-codes';

import * as express from 'express';
import User from '../src/entity/User';

import * as userService from '../services/users';
import {findUserById, isLogged} from '../services/users';
import * as bcrypt from "bcrypt";
import * as crypto from "crypto";
import * as roleService from "../services/roles";
import {authorize} from "../middleware/authorize";
import Role from "../src/entity/Role";

const urlencodedParser = bodyParser.urlencoded({extended : false});
const router = express.Router();

const getUser = async (req, res) => {
    try {
        const user: User = await userService.findUserById(req.params.id);
        const dataUser: User = await findUserById(req.session.user.id);

        if (!user) {
            res.render('error', {
                code : HttpStatus.NOT_FOUND,
                message : 'User not found!',
            });
            return;
        }
        res.render('admin/user/single', {
            user,
            userData : dataUser,
            isLogged : isLogged(req)
        });
    } catch (exception) {
        res.render('error', {
            code : HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message,
        });
    }
};

const getUserEdit = async (req, res) => {
    try {
        const user: User = await userService.findUserById(req.params.id);
        const dataUser: User = await findUserById(req.session.user.id);
        const roles: Role[] = await roleService.findRoles();

        if (!user) {
            res.render('error', {
                code : HttpStatus.NOT_FOUND,
                userData : req.session.user,
                message : 'Nie znaleziono użytkownika!',
            });
            return;
        }
        res.render('admin/user/edit', {
            roles,
            user,
            message : '',
            userData : dataUser,
            isLogged : isLogged(req)
        });
    } catch (exception) {
        res.render('error', {
            code : HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message,
        });
    }
};

const getUsers = async (req, res) => {
    try {
        const users: User[] = await userService.findUsers();
        const dataUser: User = await findUserById(req.session.user.id);

        res.render('admin/index', {
            users,
            userData : dataUser,
            isLogged : isLogged(req)
        });
    } catch (exception) {
        res.render('error', {
            code : HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message
        });
    }
};

const createUser = async (req, res) => {
    try {
        const schemaRules = {
            email : Joi.string().email({minDomainAtoms : 2}).required(),
            firstName : Joi.string().min(3).max(50).required(),
            lastName : Joi.string().min(3).max(50).required(),
            password : Joi.string().required().strict(),
            confirmPassword : Joi.string().valid(Joi.ref('password')).required().strict(),
            token : Joi.string().alphanum(),
        };

        const schema = Joi.object(schemaRules).with('password', 'confirmPassword');
        const dataUser: User = await findUserById(req.session.user.id);

        const options = {
            abortEarly : false,
            allowUnknown : true,
            stripUnknown : true
        };

        const {error, value} = schema.validate(req.body, options);
        if (error) {
            res.render('admin/user/create', {
                code : HttpStatus.BAD_REQUEST,
                message : error.details[0].message,
                userData : dataUser,
                isLogged : isLogged(req)
            });
            return;
        } else {
            const user: User = await userService.findUserByEmail(req.body.email);

            bcrypt.hash(req.body.password, 10, (err, hash) => {
                const newUser: any = new User(null, req.body.email, req.body.firstName, req.body.lastName, hash,
                    crypto.randomBytes(16).toString('hex'));
                if (user) {
                    return res.render('admin/user/create', {
                        code : HttpStatus.BAD_REQUEST,
                        message : 'Użytkownik istnieje już',
                        userData : dataUser,
                        isLogged : isLogged(req)
                    });
                }

                userService.saveUser(newUser, [req.body.role]);

                res.redirect('/admin');
            });
        }

    } catch (exception) {
        res.render('error', {
            code : HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message,
        });
    }
};

const updateUser = async (req, res) => {
    try {
        const schemaRules = {
            email : Joi.string().email({minDomainAtoms : 2}).required(),
            firstName : Joi.string().min(3).max(50).required(),
            lastName : Joi.string().min(3).max(50).required(),
            password : Joi.string().required().strict(),
            confirmPassword : Joi.string().valid(Joi.ref('password')).required().strict(),
            token : Joi.string().alphanum(),
        };

        const schema = Joi.object(schemaRules).with('password', 'confirmPassword');
        const dataUser: User = await findUserById(req.session.user.id);

        const options = {
            abortEarly : false,
            allowUnknown : true,
            stripUnknown : true
        };

        const {error, value} = schema.validate(req.body, options);
        if (error) {
            res.render('admin/user/edit', {
                code : HttpStatus.BAD_REQUEST,
                message : error.details[0].message,
                userData : dataUser,
                isLogged : isLogged(req)
            });
            return;
        } else {
            const user: User = await userService.findUserByEmail(req.body.email);
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                const updateUser: any = new User(user._id, req.body.email, req.body.firstName, req.body.lastName, hash,
                    crypto.randomBytes(16).toString('hex'));

                userService.updateUser(updateUser, req.body.role);

                res.redirect('/admin');
            });
        }

    } catch (exception) {
        res.render('error', {
            code : HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message,
        });
    }
};

const removeUser = async (req, res) => {
    try {
        const user: User = await userService.findUserById(req.body.id);

        await userService.removeUser(user._id);

        res.redirect('/admin');

    } catch (exception) {
        res.render('error', {
            code : HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message,
        });
    }
};

const showUserForm = async (req, res) => {
    try {
        const roles: Role[] = await roleService.findRoles();
        const dataUser: User = await findUserById(req.session.user.id);

        res.render('admin/user/create', {
            roles,
            message : '',
            userData : dataUser,
            isLogged : isLogged(req),
        });
    } catch (exception) {
        res.render('error', {
            code : HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message,
        });
    }
};

router.post('/user/:id/delete', urlencodedParser, removeUser);
router.post('/user/:id/update', urlencodedParser, updateUser);
router.post('/', urlencodedParser, createUser);
router.get('/dodaj', [authorize], showUserForm);
router.get('/', [authorize], getUsers);
router.get('/user/:id', [authorize], getUser);
router.get('/user/:id/edit', [authorize], getUserEdit);

export default router;
