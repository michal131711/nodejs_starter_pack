import * as bodyParser from 'body-parser';
import * as Joi from 'joi';
import * as HttpStatus from 'http-status-codes';

import * as express from 'express';
import User from '../src/entity/User';

import {authorize} from '../middleware/authorize';

import * as userService from '../services/users';
import {findUserById, isLogged} from '../services/users';
import * as bcrypt from "bcrypt";
import * as crypto from "crypto";
import nodemailer from "nodemailer";
import ILoginData from "../interfaces/ILoginData";

const urlencodedParser = bodyParser.urlencoded({extended : false});
const router = express.Router();

const newPassword = async (req, res) => {
    try {
        const schemaRules = {
            password : Joi.string().required(),
            confirmPassword : Joi.string().valid(Joi.ref('password')).required().strict(),
        };

        const plainText = req.body.password;
        const schema = Joi.object(schemaRules).with('password', 'confirmPassword');
        const options = {
            abortEarly : false,
            allowUnknown : true,
            stripUnknown : true
        };
        const {error, value} = schema.validate(req.body, options);

        if (error) {
            res.render('error', {
                code : HttpStatus.BAD_REQUEST,
                message : error.details[0].message
            });
            return;
        } else {
            const user: User = await userService.findUserByEmail(req.body.email);
            bcrypt.hash(plainText, 10, (err, hash) => {
                const passwordData: any = {
                    email : user._email,
                    firstName : user._firstName,
                    lastName : user._lastName,
                    password : hash
                };

                userService.updateUser(passwordData);

                res.redirect('/login');
            });
        }

    } catch (exception) {
        res.render('error', {
            code : HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message,
        });
    }
}

const changePassword = async (req, res) => {
    try {

        const changePassword: any = {
            email : req.body.email,
        };

        const user: User = await userService.doesUserExist(changePassword);

        const transport = nodemailer.createTransport({
            host : 'smtp.mailtrap.io',
            port : 2525,
            auth : {
                user : process.env.MAIL_USER,
                pass : process.env.MAIL_PASS
            }
        });

        const message = {
            from : process.env.MAIL_FROM,
            to : user._email,
            subject : 'RESET HASŁA',
            html : `<a href="/${user._token}/reset/${user._email}"> Przejdż do zmiany hasła</a>` // Plain text body
        };

        transport.sendMail(message, (err, info) => {
            if (err) {
                console.log(err)
            } else {
                console.log(info);
            }
        });
    } catch (exception) {
        res.render('error', {
            code : HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message,
        });
    }
}

const createUser = async (req, res) => {
    try {
        const schemaRules = {
            email : Joi.string().email({minDomainAtoms : 2}).required(),
            firstName : Joi.string().min(3).max(50).required(),
            lastName : Joi.string().min(3).max(50).required(),
            password : Joi.string().required().strict(),
            confirmPassword : Joi.string().valid(Joi.ref('password')).required().strict(),
            token : Joi.string().alphanum(),
        };

        const schema = Joi.object(schemaRules).with('password', 'confirmPassword');

        const options = {
            abortEarly : false,
            allowUnknown : true,
            stripUnknown : true
        };

        const {error, value} = schema.validate(req.body, options);
        if (error) {
            res.render('users/create', {
                code : HttpStatus.BAD_REQUEST,
                message : error.details[0].message,
                isLogged : isLogged(req)
            });
            return;
        } else {
            const user: User = await userService.findUserByEmail(req.body.password);

            bcrypt.hash(req.body.password, 10, (err, hash) => {

                const newUser: any = new User(
                    null,
                    req.body.email,
                    req.body.firstName,
                    req.body.lastName,
                    hash,
                    crypto.randomBytes(16).toString('hex')
                );

                if (user) {
                    return res.render('users/create', {
                        code : HttpStatus.BAD_REQUEST,
                        message : 'Użytkownik istnieje już',
                        isLogged : isLogged(req)
                    });
                }
                userService.saveUser(newUser, [3]);

                res.redirect('/login');
            });
        }

    } catch (exception) {
        res.render('error', {
            code : HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message,
        });
    }
};

const login = async (req, res) => {
    try {
        const schema = {
            email : Joi.string().email({minDomainAtoms : 2}).required(),
            password : Joi.string().required()
        };
        const loginData: ILoginData = {
            email : req.body.email,
            password : req.body.password,
        };

        const user: User = await userService.doesUserExist(loginData);

        const {error, value} = Joi.validate(loginData, schema);
        if (error) {
            return res.render('users/login', {
                code : HttpStatus.BAD_REQUEST,
                message : error.details[0].message,
                isLogged : isLogged(req)
            });
        }

        if (user) {
            console.log(user._password)
            bcrypt.compare(loginData.password, user._password, (err, result) => {
                              if (result === true) {
                    req.session.user = user;

                    return res.redirect('/home');
                } else {
                    return res.render('users/login', {
                        code : HttpStatus.BAD_REQUEST,
                        isLogged : isLogged(req),
                        message : 'Nie prawidłowe hasło'
                    });
                }
            });
        }

    } catch (exception) {
        res.render('users/login', {
            code : exception.code || HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message,
        });
    }
};

const showUserForm = (req, res) => {
    try {
        res.render('users/create', {
            message : '',
            isLogged : isLogged(req),
        });
    } catch (exception) {
        res.render('error', {
            code : HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message,
        });
    }
};

const showUserPasswordResetForm = (req, res) => {
    try {
        res.render('users/change_password', {
            data : req.params,
            message : '',
            isLogged : isLogged(req),
        });
    } catch (exception) {
        res.render('error', {
            code : HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message,
        });
    }
};

const showUserResetPasswordForm = (req, res) => {
    try {
        res.render('users/reset', {
            message : '',
            isLogged : isLogged(req)
        });
    } catch (exception) {
        res.render('error', {
            code : HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message,
        });
    }
};

const showLoginForm = (req, res) => {
    try {
        res.render('users/login', {
            message : '',
            logout : req.query.logout,
            isLogged : isLogged(req)
        });
    } catch (exception) {
        res.render('error', {
            code : HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message,
        });
    }
};

const showHome = async (req, res) => {
    try {
        const dataUser: User = await findUserById(req.session.user.id);

        res.render('home', {
            logout : req.query.logout,
            userdata : dataUser,
            isLogged : isLogged(req)
        });
    } catch (exception) {
        res.render('error', {
            userdata : req.session.user,
            code : HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message,
        });
    }
};

const showStartHome = (req, res) => {
    try {
        res.render('index');
    } catch (exception) {
        res.render('error', {
            code : HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message,
        });
    }
};

const logout = (req, res) => {
    req.session.destroy(() => {
        res.redirect('/login?logout=true');
    });
};

router.post('/', urlencodedParser, createUser);
router.post('/login/set', urlencodedParser, login);
router.post('/login/reset', urlencodedParser, changePassword);
router.post('/login/new/password', urlencodedParser, newPassword);
router.get('/', showStartHome);
router.get('/register', showUserForm);
router.get('/reset', showUserResetPasswordForm);
router.get('/:token/reset/:email', showUserPasswordResetForm);
router.get('/login', showLoginForm);
router.get('/home', showHome);
router.get('/logout', [authorize], logout);

export default router;
