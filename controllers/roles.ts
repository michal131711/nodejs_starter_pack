import * as bodyParser from 'body-parser';
import * as Joi from 'joi';
import * as HttpStatus from 'http-status-codes';

import * as express from 'express';
import {findRoleById, findRoleByName, findRoles, saveRole} from '../services/roles';

import {authorize} from '../middleware/authorize';
import {hasPermission, isAdmin} from '../middleware/permission';

import Role from '../src/entity/Role';
import {findUserById, hasRights, isLogged} from "../services/users";
import Roles from "../enums/Roles";
import User from "../src/entity/User";

const urlencodedParser = bodyParser.urlencoded({extended : false});

const router = express.Router();

const getRole = async (req, res) => {
    try {
        const role: any = await findRoleById(req.params.id);
        const dataUser: User = await findUserById(req.session.user.id);

        if (!role) {
            res.render('error', {
                code : HttpStatus.NOT_FOUND,
                message : 'Role not found!',
            });
            return;
        }
        res.render('admin/roles/single', {
            role,
            logout : req.query.logout,
            userData : dataUser,
            isLogged : isLogged(req)
        });
    } catch (exception) {
        res.render('error', {
            code : HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message,
        });
    }
};

const getRoles = async (req, res) => {
    try {
        const roles: Role[] = await findRoles();
        const dataUser: User = await findUserById(req.session.user.id);

        const showButton: boolean = hasRights(req.session.user, [
            Roles.ROLE_ADMIN,
        ]);
        res.render('admin/roles/list', {
            roles,
            showButton,
            logout : req.query.logout,
            userData : dataUser,
            isLogged : isLogged(req)
        });
    } catch (exception) {
        res.render('error', {
            code : HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message,
        });
    }
};

const createRole = async (req, res) => {
    try {
        const schema = {
            _name : Joi.string().regex(/^ROLE_(CLIENT|ADMIN|EMPLOYEE)$/).required(),
        };

        const newRole: any = new Role(null, req.body.name);
        const dataUser: User = await findUserById(req.session.user.id);

        const {error, value} = Joi.validate(newRole, schema);
        if (error) {
            res.render('admin/roles/create', {
                message : error.details[0].message,
                logout : req.query.logout,
                userData : dataUser,
                isLogged : isLogged(req)
            });
            return;
        }

        const role: Role = await findRoleByName(newRole._name);
        if (role) {
            throw {
                message : "Rola istnieje",
                code : HttpStatus.CONFLICT
            };
        }

        await saveRole(newRole);
        res.redirect('/admin/role');

    } catch (exception) {
        res.render('error', {
            code : exception.code || HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message,
        });
    }
};

const showRoleForm = async (req, res) => {
    try {
        const dataUser: User = await findUserById(req.session.user.id);

        res.render('admin/roles/create', {
            logout : req.query.logout,
            userData : dataUser,
            isLogged : isLogged(req)
        });
    } catch (exception) {
        res.render('error', {
            code : exception.code || HttpStatus.INTERNAL_SERVER_ERROR,
            message : exception.message,
        });
    }
};

router.post('/', [urlencodedParser, authorize, isAdmin, hasPermission], createRole);
router.get('/create', [isAdmin, hasPermission, authorize], showRoleForm);
router.get('/:id', [authorize], getRole);
router.get('/', [authorize], getRoles);

export default router;
