import * as bcrypt from "bcrypt";
import * as crypto from "crypto";
import {Connection, ConnectionManager, ConnectionOptions, createConnection, getConnectionManager} from "typeorm";
import Role from "../src/entity/Role";
import User from "../src/entity/User";
import {config} from "dotenv";

config();

export class Database {
    private connectionManager: ConnectionManager;

    constructor() {
        this.connectionManager = getConnectionManager();
    }

    public async getConnection(name: string): Promise<Connection> {
        const CONNECTION_NAME: string = name;
        let connection: Connection;
        const hasConnection = this.connectionManager.has(CONNECTION_NAME);
        if (hasConnection) {
            connection = this.connectionManager.get(CONNECTION_NAME);
            if (!connection.isConnected) {
                connection = await connection.connect();
            }
        } else {

            const connectionOptions: ConnectionOptions = {
                name : 'default',
                type : "mysql",
                host : process.env.DB_HOST,
                port : 3306,
                username : process.env.DB_USER,
                password : process.env.DB_PASS,
                database : process.env.DB_NAME,
                synchronize : false,
                logging : true,
                entities : [User, Role],
                migrations : ['src/migration/**/*.ts'],
                subscribers : ['src/subscriber/**/*.ts'],
                cli : {
                    entitiesDir : 'src/entity',
                    migrationsDir : 'src/migration',
                    subscribersDir : 'src/subscriber',
                }
            };
            connection = await createConnection(connectionOptions);
        }
        return connection;
    }
}

createConnection().then(async connection => {
        const roleAdmin = new Role(1, 'ROLE_ADMIN');
        const roleSubscriber = new Role(2, 'ROLE_SUBSCRIBER');
        const roleClient = new Role(3, 'ROLE_CLIENT');

        const getRole = await connection.manager.save(Role, roleAdmin);
        await connection.manager.save(Role, roleSubscriber);
        await connection.manager.save(Role, roleClient);

        bcrypt.hash('testowe1', 10, async (err, hash) => {

            const user = new User(
                1,
                'michal-kukula@webbuildercompany.com',
                'Michał',
                'Kukuła',
                hash,
                crypto.randomBytes(16).toString('hex'),
            )

            user._roles = [getRole];
            await connection.manager.save(User, user);
        });

    }
);
