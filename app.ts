import "reflect-metadata";
import * as express from "express";

import homeRouter from './controllers/home';
import usersRouter from './controllers/users';
import rolesRouter from './controllers/roles';
import * as logger from "morgan";
import * as cookieParser from "cookie-parser";
import * as path from "path";
import * as session from "express-session";
import * as fileUpload from "express-fileupload";
import * as cors from "cors";
import * as moment from "moment";

const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(fileUpload({
    createParentPath : true
}));

app.use(session({
    secret : 'keyboard cat',
    saveUninitialized : true,
    cookie : {maxAge : 1000 * 60 * 60 * 24},
    resave : false,
}));
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended : true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use((req, res, next) => {
    res.locals.moment = moment;
    next();
});

app.use('/', homeRouter);
app.use('/admin', usersRouter);
app.use('/admin/role', rolesRouter);

app.get('/', (req, res) => {
    res.render('index', {
        title : 'czytaj opis w serwisie!!!'
    });
});


// @ts-ignore
const port = process.env.PORT || 3000;

app.listen(port);
