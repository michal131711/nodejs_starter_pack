/**
 * enumy to typy wyliczeniowe służące do statycznej prezentacji danch predefioniowanych z góry,
 * to taki specjalny rodzaj klas
 *
 * na zajeciach pokazywałem klasę z polami statycznymi i readonly,
 * ale enum robi to sam za nas ;) więc poprawiłem na metodę łatwiejszą!
 *
 * polecam bardziej to roziązaie!
 */
enum Roles {
	ROLE_ADMIN = 'ROLE_ADMIN',
	ROLE_CLIENT = 'ROLE_CLIENT',
	ROLE_SUBSCRIBER = 'ROLE_SUBSCRIBER'
}

export default Roles
