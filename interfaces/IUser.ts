import IRole from './IRole';

export default interface IUser {
	id: number | null;
	email: string;
	firstName: string;
	lastName: string;
	password: string;
	token: string;
	roles?: IRole[]
}
