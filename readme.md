**nodemon app** - uruchamia nodemona tj. narzędzie do automatycznego restarowania serwera

**node app** - ręczne uruchomienie serwera

**ctrl+alt+l** - reformat kodu!!!

**npm install** - instaluje node_modules za pomocą pliku package.json

**npm install NAZWA_PAKIETU** - instaluje pakiet z NMP'a (npmjs.com)

**npm uninstall NAZWA_PAKIETU** - usuwa

**npm install -g** - instaluje globalnie (czyli nie załącza go jako integralną częśc projektu, np nodemon nie jest potrzebny na produkcji, więc instalacja globalna tylko na cele developowania jest dobrym pomysłem!)

**instalacja i dokumentacja Joi:**
`https://www.npmjs.com/package/joi`
`https://github.com/hapijs/joi/blob/v13.6.0/API.md`

**npm'owy pakiet mysql**
`https://www.npmjs.com/package/mysql`

**framework express**
`https://www.npmjs.com/package/express`

**tempaty po stronie widoku EJS**
`https://www.npmjs.com/package/ejs`

**body parser - parsuje body requesta na obiekty**
`https://www.npmjs.com/package/body-parser`

**kody HTTP**
`https://www.npmjs.com/package/http-status-codes`

**nodemon - restartuje serwer**
`https://www.npmjs.com/package/nodemon`

**express session - sesja usera w node.js**
`https://www.npmjs.com/package/express-session`

**dokumentacja ORMa (sequalize)**
`http://docs.sequelizejs.com`