import IRole from "../interfaces/IRole";
import Role from "../src/entity/Role";
import {Connection} from "typeorm";
import {Database} from "../config/mysql";

const database = new Database();
const connectionName = 'default';

export const findRoleById = async (id: any): Promise<Role[]> => {
    const dbConn: Connection = await database.getConnection(connectionName);

    return dbConn.manager.findByIds(Role, id);
};

export const findRoleByName = async (name: string): Promise<Role> => {
    const dbConn: Connection = await database.getConnection(connectionName);

    return dbConn.getRepository(Role).findOne({
        where : {
            name
        }
    });
};

export const findRoles = async (): Promise<Role[]> => {
    const dbConn: Connection = await database.getConnection(connectionName);

    return dbConn.getRepository(Role).find();
};

export const saveRole = async (role: IRole): Promise<Role> => {
    const dbConn: Connection = await database.getConnection(connectionName);


    const roleRepository = dbConn.getRepository(Role);

    return roleRepository.save(Role, {
        data : {
            name : role.name
        }
    });
};

// export const findRolesByUserId = (id: number): Promise<IRole[]> => {
// 	return UserRole.findAll({
// 		include: [
// 			{model: RoleData, required: true}
// 		],
// 		attributes: ['name'],
// 		where: {
// 			userId: id
// 		}
// 	});
// };
