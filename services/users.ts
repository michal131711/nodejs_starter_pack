import IUser from "../interfaces/IUser";
import ILoginData from "../interfaces/ILoginData";
import * as crypto from "crypto";
import User from "../src/entity/User";
import {Connection} from "typeorm";
import {Database} from "../config/mysql";
import {findRoleById} from "./roles";

const database = new Database();
const connectionName = 'default';
export const removeUser = async (id: number) => {
    const dbConn: Connection = await database.getConnection(connectionName);

    return dbConn.getRepository(User).delete(id);
}

export const findUserByEmail = async (email: string): Promise<User> => {
    const dbConn: Connection = await database.getConnection(connectionName);

    return dbConn.getRepository(User).findOne({
        where : {
            email
        },
        relations: ["roles"]
    });
};

export const findUserById = async (id: number): Promise<User> => {
    const dbConn: Connection = await database.getConnection(connectionName);

    return dbConn.getRepository(User).findOne({
        where : {
            id,
        },
        relations: ["roles"]
    });
};

export const findUsers = async (): Promise<User[]> => {
    const dbConn: Connection = await database.getConnection(connectionName);
    const userRepository = dbConn.getRepository(User);

    return await userRepository.find({relations : ["roles"]});
};

export const saveUser = async (user: User, idRole: number[]): Promise<any> => {
    const dbConn: Connection = await database.getConnection(connectionName);
    const userRepository = dbConn.manager;
    const getRole = await findRoleById(idRole)
    user._roles = getRole;

    return userRepository.save(User,user);
};

export const updateUser = async (user: User, roleId = 3): Promise<any> => {
    const dbConn: Connection = await database.getConnection(connectionName);
    const userRepository = dbConn.manager;
    const userData = await userRepository.findOneOrFail(User, user._id);
    const getRole = await findRoleById(roleId)
    userData._email = user._email;
    userData._firstName = user._firstName;
    userData._lastName = user._lastName;
    userData._password = user._password;
    userData._token = crypto.randomBytes(16).toString('hex');
    userData._roles = getRole;

    return await userRepository.save(userData);
};

export const doesUserExist = async (loginData: ILoginData): Promise<User> => {
    const dbConn: Connection = await database.getConnection(connectionName);

    return dbConn.getRepository(User).findOne({
        where : {
            email : loginData.email,
        }
    });
};

export const hasRights = (user: IUser, ALLOWED_ROLES: string[]): boolean => {
    if (!user || !user.roles) {
        return false;
    }

    const roles: string[] = user.roles.map(element => element.name);

    return ALLOWED_ROLES.some(element => roles.indexOf(element) !== -1);
};

export const isLogged = (req): boolean => {
    return !!(req.session && req.session.user);
};
